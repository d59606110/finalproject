package com.example.d59606110gmailcom.finalproject;

public class Contact {
    private String cdate;
    private String session, sugar_values;
    private String heart_pressure_value = null, max_pressure_value, min_pressure_value, pressure_cdate_value;
    private String weight, fat;
    private String type, item, time, cal;

    public Contact() {

    }

    public Contact(String heart_pressure_value, String max_pressure_value, String min_pressure_value, String pressure_cdate_value) {
        this.heart_pressure_value = heart_pressure_value;
        this.max_pressure_value = max_pressure_value;
        this.min_pressure_value = min_pressure_value;
        this.pressure_cdate_value = pressure_cdate_value;
    }

    public Contact(String cdate, String weight, String fat, int i, int j, int z) {
        this.cdate = cdate;
        this.weight = weight;
        this.fat = fat;
    }

    public Contact(String cdate, String session, String sugar_values) {
        this.cdate = cdate;
        this.session = session;
        this.sugar_values = sugar_values;
    }

    public Contact(String cdate, String type, String item, String time, String cal) {
        this.cdate = cdate;
        this.type = type;
        this.item = item;
        time = time.replace("小時", "");
        this.time = time;
        this.cal = cal;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getSugar_values() {
        return sugar_values;
    }

    public void setSugar_values(String sugar_values) {
        this.sugar_values = sugar_values;
    }

    public String getHeart_pressure_value() {
        return heart_pressure_value;
    }

    public void setHeart_pressure_value(String heart_pressure_value) {
        this.heart_pressure_value = heart_pressure_value;
    }

    public String getMax_pressure_value() {
        return max_pressure_value;
    }

    public void setMax_pressure_value(String max_pressure_value) {
        this.max_pressure_value = max_pressure_value;
    }

    public String getMin_pressure_value() {
        return min_pressure_value;
    }

    public void setMin_pressure_value(String min_pressure_value) {
        this.min_pressure_value = min_pressure_value;
    }

    public String getPressure_cdate_value() {
        return pressure_cdate_value;
    }

    public void setPressure_cdate_value(String pressure_cdate_value) {
        this.pressure_cdate_value = pressure_cdate_value;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCal() {
        return cal;
    }

    public void setCal(String cal) {
        this.cal = cal;
    }
}
