package com.example.d59606110gmailcom.finalproject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity {
    EditText edit_name, edit_birthday, edit_height, edit_weight, edit_HbAC1;
    Spinner edit_sex, edit_sick_type, edit_sick_age;
    Button btn_update;
    private FirebaseDatabase database;
    private DatabaseReference mDatabase;
    private String type_str;
    private String sex_str;
    private String height = null;
    private int h, integer, fraction;
    int age = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        /*左上角加入返回鍵*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_editprofile);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_birthday = (EditText) findViewById(R.id.edit_birthday);
        edit_height = (EditText) findViewById(R.id.edit_height);
        edit_HbAC1 = (EditText) findViewById(R.id.edit_HbAC1);
        edit_sex = (Spinner) findViewById(R.id.edit_sex);
        edit_sick_type = (Spinner) findViewById(R.id.edit_sick_type);
        btn_update = (Button) findViewById(R.id.btn_update);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String UID = auth.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference("user/" + UID);
        DatabaseReference reference_contacts = FirebaseDatabase.getInstance().getReference("user/" + UID);


        /*顯示原始資料*/
        ValueEventListener valueEventListener = reference_contacts.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    edit_name.setText(dataSnapshot.child("name").getValue(String.class));
                } catch (Exception ignored) {
                }
                try {
                    edit_birthday.setText(dataSnapshot.child("birthday").getValue(String.class));
                    /*計算年齡*/
                    String dataOfBirth = dataSnapshot.child("birthday").getValue(String.class);
                    assert dataOfBirth != null;
                    if (!dataOfBirth.equals("")) {
                        age = getAge(parse(dataOfBirth));
                    }
                } catch (Exception ignored) {
                }
                try {
                    edit_height.setText(dataSnapshot.child("height").getValue(String.class));
                } catch (Exception ignored) {
                }
                try {
                    edit_HbAC1.setText(dataSnapshot.child("HbAC1").getValue(String.class));
                } catch (Exception ignored) {
                }
                try {
                    /*對照資料庫資料(糖尿病類型)*/
                    int type_index;
                    final Boolean[] type_bool = {true};
                    type_str = dataSnapshot.child("sick_type").getValue(String.class);
                    String type[] = {"第一型糖尿病", "第二型糖尿病", "妊娠型糖尿病", "其他類型"};
                    assert type_str != null;
                    switch (type_str) {
                        case "第一型糖尿病":
                            type_index = 0;
                            break;
                        case "第二型糖尿病":
                            type_index = 1;
                            break;
                        case "妊娠型糖尿病":
                            type_index = 2;
                            break;
                        default:
                            type_index = 3;
                            break;
                    }
                    ArrayAdapter<String> adapter_type = new ArrayAdapter<String>(EditProfileActivity.this,
                            R.layout.support_simple_spinner_dropdown_item,
                            type);
                    edit_sick_type.setAdapter(adapter_type);
                    final int finalType_index = type_index;
                    edit_sick_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            position = finalType_index;
                            if (type_bool[0]) {
                                edit_sick_type.setSelection(position);
                                type_bool[0] = false;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (Exception e) {
                    String type[] = {"第一型糖尿病", "第二型糖尿病", "妊娠型糖尿病", "其他類型"};
                    ArrayAdapter<String> adapter_type = new ArrayAdapter<String>(EditProfileActivity.this,
                            R.layout.support_simple_spinner_dropdown_item,
                            type);
                    edit_sick_type.setAdapter(adapter_type);
                }
                try {
                    /*對照資料庫資料(性別)*/
                    final int sex_index;
                    final Boolean[] sex_bool = {true};
                    sex_str = dataSnapshot.child("sex").getValue(String.class);
                    String sex[] = {"男", "女"};
                    assert sex_str != null;
                    if (sex_str.equals("男")) {
                        sex_index = 0;
                    } else {
                        sex_index = 1;
                    }
                    ArrayAdapter<String> adapter_sex = new ArrayAdapter<String>(EditProfileActivity.this,
                            R.layout.support_simple_spinner_dropdown_item,
                            sex);
                    edit_sex.setAdapter(adapter_sex);
                    edit_sex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            position = sex_index;
                            if (sex_bool[0]) {
                                edit_sex.setSelection(position);
                                sex_bool[0] = false;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } catch (Exception e) {
                    String sex[] = {"男", "女"};
                    ArrayAdapter<String> adapter_sex = new ArrayAdapter<String>(EditProfileActivity.this,
                            R.layout.support_simple_spinner_dropdown_item,
                            sex);
                    edit_sex.setAdapter(adapter_sex);
                }
                try {
                    edit_HbAC1.setText(dataSnapshot.child("HbA1c").getValue().toString());
                } catch (Exception e) {
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        /*生日格式化*/
        edit_birthday.setInputType(InputType.TYPE_NULL);
        edit_birthday.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                edit_name.clearFocus();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showDatePickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*身高滑動輸入*/
        edit_height.setInputType(InputType.TYPE_NULL);
        edit_height.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                edit_name.clearFocus();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showNumberPickerDialog_height();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*糖化血色素滑動輸入*/
        edit_HbAC1.setInputType(InputType.TYPE_NULL);
        edit_HbAC1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                edit_name.clearFocus();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        HbAC1_value();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*資料更新按鈕的監聽*/
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_name.getText().toString().equals("")) {
                    edit_name.setError("");
                    showErrorMsg();
                } else if (edit_birthday.getText().toString().equals("")) {
                    edit_birthday.setError("");
                    showErrorMsg();
                } else if (edit_height.getText().toString().equals("")) {
                    edit_height.setError("");
                    showErrorMsg();
                } else {
                    Map<String, Object> data = new HashMap<>();
                    data.put("name", edit_name.getText().toString());
                    data.put("sex", edit_sex.getSelectedItem().toString());
                    data.put("age", age);
                    data.put("birthday", edit_birthday.getText().toString());
                    data.put("height", edit_height.getText().toString());
                    data.put("sick_type", edit_sick_type.getSelectedItem().toString());
                    data.put("HbA1c", edit_HbAC1.getText().toString());
                    mDatabase.updateChildren(data);
                    showOkMsg();
                }
            }
        });
    }

    public void showErrorMsg() {
        AlertDialog.Builder dialog_ok = new AlertDialog.Builder(EditProfileActivity.this);
        dialog_ok.setTitle("資料儲存失敗");
        dialog_ok.setMessage("請再次確認資料填寫完善");
        dialog_ok.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog_ok.show();
    }

    public void showOkMsg() {
        AlertDialog.Builder dialog_ok = new AlertDialog.Builder(EditProfileActivity.this);
        dialog_ok.setTitle("資料儲存成功提示");
        dialog_ok.setMessage("恭喜！資料儲存成功");
        dialog_ok.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog_ok.show();
        Log.d("TAG", "資料儲存成功");
    }

    public int getAge(Date birthDay) throws Exception {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) {
            throw new IllegalArgumentException("The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;
            } else {
                age--;
            }
        }
        return age;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Date parse(String strDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(strDate);
    }

    private void showDatePickerDialog() {
        Calendar c = Calendar.getInstance();
        String[] split_birth = edit_birthday.getText().toString().split("-");
        String birth_year = split_birth[0];
        String birth_month = split_birth[1];
        String birth_day = split_birth[2];
        new DatePickerDialog(EditProfileActivity.this, DatePickerDialog.THEME_HOLO_DARK
                , new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String smonth, sday;
                monthOfYear += 1;
                if (monthOfYear >= 1 && monthOfYear <= 9) {
                    smonth = "0" + Integer.toString(monthOfYear);
                } else {
                    smonth = Integer.toString(monthOfYear);
                }
                if (dayOfMonth >= 1 && dayOfMonth <= 9) {
                    sday = "0" + Integer.toString(dayOfMonth);
                } else {
                    sday = Integer.toString(dayOfMonth);
                }
                edit_birthday.setText(String.valueOf(year) + "-" + String.valueOf(smonth) + "-" + String.valueOf(sday));
            }
        }, Integer.parseInt(birth_year), Integer.parseInt(birth_month) - 1, Integer.parseInt(birth_day)).show();
    }

    public void showNumberPickerDialog_height() {
        final AlertDialog.Builder number_dialog = new AlertDialog.Builder(EditProfileActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        number_dialog.setTitle("身高")
                .setView(dialogView);
        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.dialog_number_picker);
        h = Integer.valueOf(edit_height.getText().toString());
        Log.i("TAG", String.valueOf(h));
        numberPicker.setMaxValue(250);
        numberPicker.setMinValue(130);
        numberPicker.setValue(h);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                h = newVal;
            }
        });
        number_dialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                edit_height.setText(String.valueOf(h));
            }
        });
        number_dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        number_dialog.show();
    }

    private void HbAC1_value() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        final LayoutInflater inflater = this.getLayoutInflater();
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.double_number_picker, null);

        /*宣告Layout變數*/
        final NumberPicker integer_picker = (NumberPicker) view.findViewById(R.id.integer_picker);
        final NumberPicker fraction_picker = (NumberPicker) view.findViewById(R.id.fraction_picker);

        /*預設值字串處理*/
        String hbAC1 = edit_HbAC1.getText().toString();
        String text[] = new String[2];
        try {
            text = hbAC1.split("\\.");
            integer = Integer.valueOf(text[0]);
            fraction = Integer.valueOf(text[1]);
        } catch (Exception e) {
            integer = 6;
            fraction = 0;
        }


        builder.setTitle("糖化血色素").setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        edit_HbAC1.setText(String.valueOf(integer) + "." + String.valueOf(fraction));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        /*整數部分設置*/
        integer_picker.setMaxValue(20);
        integer_picker.setMinValue(0);
        integer_picker.setValue(integer);
        integer_picker.setWrapSelectorWheel(false);

        /*小數部分設置*/
        fraction_picker.setMaxValue(9);
        fraction_picker.setMinValue(0);
        fraction_picker.setValue(fraction);

        integer_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                integer = newVal;
            }
        });
        fraction_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                fraction = newVal;
            }
        });
        builder.show();
    }
}
