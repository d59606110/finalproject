package com.example.d59606110gmailcom.finalproject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;

public class ExerciseActivity extends AppCompatActivity {
    EditText exercise_cdate_value, exercise_ctime_value;
    Spinner spinner_exercise_type, spinner_exercise_item, spinner_exercise_time;
    Button btn_add_exercise, btn_inquire_exercise;
    String UID = null;
    double cal = 0;
    private int year, month, day, hour, minute;
    private String smonth, sday, si, sil;
    private int kgs;
    private Double double_time = 0.0;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase, eDatabase;
    final ArrayList<String> o2_exercise = new ArrayList<String>();
    final ArrayList<String> no_o2_exercise = new ArrayList<String>();
    private String exercise_item = null, exercise_type_name = null, exercise_time = null, cal_1 = null;
    String get_exercise_time = null;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        /*左上角加入返回鍵*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_exercise);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*給予參數*/
        exercise_cdate_value = (EditText) findViewById(R.id.exercise_cdate_value);
        exercise_ctime_value = (EditText) findViewById(R.id.exercise_ctime_value);
        spinner_exercise_item = (Spinner) findViewById(R.id.spinner_exercise_item);
        spinner_exercise_time = (Spinner) findViewById(R.id.spinner_exercise_time);
        spinner_exercise_type = (Spinner) findViewById(R.id.spinner_exercise_type);
        btn_add_exercise = (Button) findViewById(R.id.btn_add_exercise);
        btn_inquire_exercise = (Button) findViewById(R.id.btn_inquire_exercise);
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("user");
        eDatabase = FirebaseDatabase.getInstance().getReference("exercise");

        UID = auth.getUid();


        getTime();
        /*日期統一格式輸入*/
        exercise_cdate_value.setInputType(InputType.TYPE_NULL);
        exercise_cdate_value.setText(year + "-" + smonth + "-" + sday);
        exercise_cdate_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showDatePickerDialog();
                }
                return true;
            }
        });

        /*時間統一格式輸入*/
        exercise_ctime_value.setInputType(InputType.TYPE_NULL);
        exercise_ctime_value.setText(si + ":" + sil);
        exercise_ctime_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showTimePickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*運動類型*/
        final ArrayList<String> exercise_type = new ArrayList<String>();
        exercise_type.add("有氧運動");
        exercise_type.add("無氧運動");
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(ExerciseActivity.this,
                R.layout.support_simple_spinner_dropdown_item,
                exercise_type);
        spinner_exercise_type.setAdapter(adapter);
//        spinner_exercise_type.setSelection(0, true);

        /*放運動項目進去Adapter*/
        final ArrayAdapter<String> adapter_item_0 = new ArrayAdapter<String>(ExerciseActivity.this,
                R.layout.support_simple_spinner_dropdown_item,
                o2_exercise);
        spinner_exercise_item.setAdapter(adapter_item_0);

        /*有氧無氧的監聽，並且控制運動類型的顯示*/
        spinner_exercise_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("TAG", "Spinner_item_position " + String.valueOf(position));
                try {
                    if (position == 0) {
                        spinner_exercise_item.setAdapter(adapter_item_0);
                        exercise_type_name = exercise_type.get(position);
                    } else {
                        ArrayAdapter<String> adapter_item_1 = new ArrayAdapter<String>(ExerciseActivity.this,
                                R.layout.support_simple_spinner_dropdown_item,
                                no_o2_exercise);
                        spinner_exercise_item.setAdapter(adapter_item_1);
                        exercise_type_name = exercise_type.get(position);
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        /*有氧運動種類*/
        eDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.i("TAG", "Start get values");
                for (DataSnapshot ds : dataSnapshot.child("有氧運動").getChildren()) {
                    o2_exercise.add(ds.getKey());
                }
                Log.i("TAG", "End get values, " + String.valueOf(o2_exercise.size()));
                spinner_exercise_item.setAdapter(adapter_item_0);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        /*無氧運動種類*/
        eDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.child("無氧運動").getChildren()) {
                    no_o2_exercise.add(ds.getKey());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        /*運動項目的選取監聽*/
        spinner_exercise_item.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (exercise_type_name.equals("有氧運動")) {
                    exercise_item = o2_exercise.get(position);
                    Log.i("TAG", exercise_item);
                    /*運動消耗卡路里讀取*/
                    eDatabase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            try {
                                cal_1 = dataSnapshot.child(exercise_type_name).child(exercise_item).child(get_exercise_time)
                                        .getValue().toString();
                                Log.i("TAG", cal_1);
                            } catch (Exception e) {
                                Log.i("TAG", e.toString());
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                } else {
                    exercise_item = no_o2_exercise.get(position);
                    Log.i("TAG", exercise_item);
                    /*運動消耗卡路里讀取*/
                    eDatabase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            try {
                                cal_1 = dataSnapshot.child(exercise_type_name).child(exercise_item).child(get_exercise_time)
                                        .getValue().toString();
                                Log.i("TAG", cal_1);
                            } catch (Exception e) {
                                Log.i("TAG", e.toString());
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*運動時間*/
        String exercise_time[] = {"0.5小時", "1小時", "1.5小時", "2小時"};
        final Double exercise_time_values[] = {0.5, 1.0, 1.5, 2.0};
        final String exercise_kg_value[] = {"40", "50", "60", "70"};
        ArrayAdapter<String> adapter_time = new ArrayAdapter<String>(ExerciseActivity.this,
                R.layout.support_simple_spinner_dropdown_item,
                exercise_time);
        spinner_exercise_time.setAdapter(adapter_time);

        /*使用者體重讀取*/
        mDatabase.addValueEventListener(new ValueEventListener() {
            String kg_in_database = null, key = null;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.child(UID).child("weightdata").getChildren()) {
                    key = ds.getKey();
                }
                kg_in_database = dataSnapshot.child(UID).child("weightdata").child(key).child("weight").getValue().toString();
                Log.i("TAG", kg_in_database);
                kgs = (int) Math.floor(Double.valueOf(kg_in_database));
                Log.i("TAG", String.valueOf(kgs));

                Log.i("TAG", "kgs:" + String.valueOf(kgs));
                if (kgs < 50) {
                    get_exercise_time = exercise_kg_value[0];
                } else if (kgs >= 50 && kgs < 60) {
                    get_exercise_time = exercise_kg_value[1];
                } else if (kgs >= 60 && kgs < 70) {
                    get_exercise_time = exercise_kg_value[2];
                } else {
                    get_exercise_time = exercise_kg_value[3];
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        /*運動時間轉換成可計算值*/
        spinner_exercise_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                double_time = exercise_time_values[position];
//                Log.i("TAG", get_exercise_time);
                /*運動消耗卡路里讀取*/
                eDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        try {
                            cal_1 = dataSnapshot.child(exercise_type_name).child(exercise_item).child(get_exercise_time)
                                    .getValue().toString();
                            Log.i("TAG", "1." + cal_1);
                        } catch (Exception e) {
                            Log.i("TAG", "1." + e.toString());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*運動資料存進Firebase Realtime Database*/
        btn_add_exercise.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Log.i("TAG", cal_1);
                                                    cal = Double.valueOf(cal_1) * double_time;
                                                    Log.i("TAG", "cal:" + String.valueOf(cal));
                                                    if (exercise_cdate_value.getText().toString().equals("") ||
                                                            exercise_ctime_value.getText().toString().equals("") ||
                                                            spinner_exercise_type.getSelectedItem().toString().equals("") ||
                                                            spinner_exercise_item.getSelectedItem().toString().equals("") ||
                                                            spinner_exercise_time.getSelectedItem().toString().equals("")) {
                                                        AlertDialog.Builder dialog_reset = new AlertDialog.Builder(ExerciseActivity.this);
                                                        dialog_reset.setTitle("資料新增失敗");
                                                        dialog_reset.setMessage("請在確認一次資料是否填寫完整");
                                                        dialog_reset.setPositiveButton("再試一次", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {

                                                            }
                                                        });
                                                        dialog_reset.show();
                                                    } else {
                                                        Contact user = new Contact((exercise_cdate_value.getText().toString() + " " + exercise_ctime_value.getText().toString()),
                                                                spinner_exercise_type.getSelectedItem().toString(),
                                                                spinner_exercise_item.getSelectedItem().toString(),
                                                                spinner_exercise_time.getSelectedItem().toString(),
                                                                String.valueOf((int) cal));
                                                        mDatabase.child(UID).child("exercise").push().setValue(user);
                                                        AlertDialog.Builder dialog_ok = new AlertDialog.Builder(ExerciseActivity.this);
                                                        dialog_ok.setTitle("資料新增成功提示");
                                                        dialog_ok.setMessage("恭喜！資料新增成功");
                                                        dialog_ok.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {

                                                            }
                                                        });
                                                        dialog_ok.show();
//                                                        exercise_cdate_value.setText("");
//                                                        exercise_ctime_value.setText("");
                                                        Log.d("TAG", "新增資料成功");
                                                    }
                                                }
                                            }
        );

        /*跳轉頁面到運動查詢*/
        btn_inquire_exercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(
                        ExerciseActivity.this,
                        InquireExerciseActivity.class
                ), 1);
            }
        });
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(ExerciseActivity.this, DatePickerDialog.THEME_HOLO_DARK
                , new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String smonth, sday, si, sil;
                if (monthOfYear >= 1 && monthOfYear <= 9) {
                    smonth = "0" + Integer.toString(monthOfYear);
                } else {
                    smonth = Integer.toString(monthOfYear);
                }
                if (dayOfMonth >= 1 && dayOfMonth <= 9) {
                    sday = "0" + Integer.toString(dayOfMonth);
                } else {
                    sday = Integer.toString(dayOfMonth);
                }
                exercise_cdate_value.setText(year + "-" + smonth + "-" + sday);
            }
        }, year, month, day).show();
    }

    private void showTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        new TimePickerDialog(ExerciseActivity.this, TimePickerDialog.THEME_HOLO_DARK
                , new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                String si, sil;
                if (i >= 0 && i <= 9) {
                    si = "0" + Integer.toString(i);
                } else {
                    si = Integer.toString(i);
                }
                if (i1 >= 0 && i1 <= 9) {
                    sil = "0" + Integer.toString(i1);
                } else {
                    sil = Integer.toString(i1);
                }
                exercise_ctime_value.setText(si + ":" + sil);
            }
        }, hour, minute, false).show();
    }

    private void getTime() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        if (month >= 1 && month <= 9) {
            smonth = "0" + Integer.toString(month);
        } else {
            smonth = Integer.toString(month);
        }
        if (day >= 1 && day <= 9) {
            sday = "0" + Integer.toString(day);
        } else {
            sday = Integer.toString(day);
        }
        if (hour >= 0 && hour <= 9) {
            si = "0" + Integer.toString(hour);
        } else {
            si = Integer.toString(hour);
        }
        if (minute >= 0 && minute <= 9) {
            sil = "0" + Integer.toString(minute);
        } else {
            sil = Integer.toString(minute);
        }
    }
}
