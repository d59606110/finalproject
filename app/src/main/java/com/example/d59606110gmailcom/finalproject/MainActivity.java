package com.example.d59606110gmailcom.finalproject;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ArrayAdapter<String> adapter;
    private static final int RC_SIGN_IN = 123;
    ImageView img_sugar;
    ImageView img_exercise;
    ImageView img_pressure;
    ImageView img_profile, image_logo, img_weight;
    ImageView img_food, img_alarm;
    Button btn_signout;
    List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.GoogleBuilder().build(),
//            new AuthUI.IdpConfig.FacebookBuilder().build(),
            new AuthUI.IdpConfig.EmailBuilder().build()
//            new AuthUI.IdpConfig.PhoneBuilder().build()
    );
    String UID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img_sugar = (ImageView) findViewById(R.id.img_sugar);
        img_exercise = (ImageView) findViewById(R.id.img_exercise);
        img_pressure = (ImageView) findViewById(R.id.img_pressure);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        image_logo = (ImageView) findViewById(R.id.image_logo);
        img_weight = (ImageView) findViewById(R.id.img_weight);
        img_food = (ImageView) findViewById(R.id.img_food);

        mAuth = FirebaseAuth.getInstance();

        /*驗證登入狀態*/
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("TAG", "onAuthStateChanged:signed_in:" + user.getUid());
                    UID = user.getUid();
                } else {
                    // User is signed out
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false, true)
                                    .setAvailableProviders(providers)
                                    .setTheme(R.style.GreenTheme)
                                    .setLogo(R.drawable.logo)
                                    .build(),
                            RC_SIGN_IN);
                    Log.d("TAG", "onAuthStateChanged:signed_out");
                }
            }
        };

        /*點擊圖片跳轉至網頁*/
        image_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://sugarmanagement.tk/");
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        });

        /*跳轉到血糖管理頁面*/
        img_sugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(
                        MainActivity.this,
                        SugarActivity.class
                ), 1);
                Log.d("TAG", "跳轉頁面到血糖管理");
            }
        });

        /*跳轉到運動管理頁面*/
        img_exercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(
                        MainActivity.this,
                        ExerciseActivity.class
                ), 1);
            }
        });

        /*跳轉到血壓管理頁面*/
        img_pressure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(
                        MainActivity.this,
                        PressureActivity.class
                ), 1);
            }
        });

        /*跳轉到體重管理頁面*/
        img_weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,
                        WeightActivity.class));
            }
        });

        /*跳轉到飲食知識頁面*/
        img_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "功能尚未開放，敬請期待！", Snackbar.LENGTH_LONG)
                        .setAction("action", null).show();
            }
        });

        /*跳轉到個人頁面*/
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(
                        MainActivity.this,
                        ProfileActivity.class
                ), 1);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}
