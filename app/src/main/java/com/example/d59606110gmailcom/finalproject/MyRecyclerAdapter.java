package com.example.d59606110gmailcom.finalproject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    final ArrayList<Contact> list;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private String UID = null;

    public MyRecyclerAdapter(ArrayList<Contact> list) {
        this.list = list;
    }

    @Override
    public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, final int position) {
        Contact contact = list.get(position);
        holder.setValues(contact);

        auth = FirebaseAuth.getInstance();
        UID = auth.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference("user/" + UID + "/blood_sugar");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder dialog_onclick = new AlertDialog.Builder(v.getContext());
                dialog_onclick.setTitle("資料");
//                list.get(position).getSugar_values()  取得血糖資料
                dialog_onclick.setMessage("是否要刪除這筆資料？");
                dialog_onclick.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog_onclick.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final Boolean[] ch = {true};
                        mDatabase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                ArrayList<String> childKey_list = new ArrayList<>();
                                childKey_list.clear();
                                for (DataSnapshot p : dataSnapshot.getChildren()) {
                                    String childKey = p.getKey();
                                    childKey_list.add(childKey);
                                }
                                if (ch[0]) {
                                    String delete_childID = childKey_list.get(position);
                                    mDatabase.child(delete_childID).removeValue();
                                    ch[0] = false;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });
                dialog_onclick.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView sugar_date;
        final TextView sugar_value;
        final TextView sugar_type;


        public ViewHolder(View itemView) {
            super(itemView);
            sugar_date = (TextView) itemView.findViewById(R.id.sugar_date);
            sugar_type = (TextView) itemView.findViewById(R.id.sugar_type);
            sugar_value = (TextView) itemView.findViewById(R.id.sugar_value);
        }

        public void setValues(Contact contact) {
            String date = contact.getCdate();
            String dd[] = {"年", "月", "日"};
            date = date.replaceFirst("-", dd[0]);
            date = date.replaceFirst("-", dd[1]);
            date = date.replaceFirst(" ", dd[2] + "  ");
            sugar_date.setText(date);
            sugar_type.setText(contact.getSession());
            sugar_value.setText(contact.getSugar_values());
        }
    }
}
