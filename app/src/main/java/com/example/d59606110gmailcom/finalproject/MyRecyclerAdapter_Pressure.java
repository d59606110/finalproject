package com.example.d59606110gmailcom.finalproject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MyRecyclerAdapter_Pressure extends RecyclerView.Adapter<MyRecyclerAdapter_Pressure.ViewHolder> {
    final ArrayList<Contact> list;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private String UID = null;

    public MyRecyclerAdapter_Pressure(ArrayList<Contact> list) {
        this.list = list;
    }

    @Override
    public MyRecyclerAdapter_Pressure.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_pressure, parent, false);
        MyRecyclerAdapter_Pressure.ViewHolder viewHolder = new MyRecyclerAdapter_Pressure.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecyclerAdapter_Pressure.ViewHolder holder, final int position) {
        Contact contact = list.get(position);
        holder.setValues(contact);

        auth = FirebaseAuth.getInstance();
        UID = auth.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference("user/" + UID + "/blood_pressure");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder dialog_onclick = new AlertDialog.Builder(v.getContext());
                dialog_onclick.setTitle("資料");
                dialog_onclick.setMessage("是否要刪除這筆資料？");
                dialog_onclick.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog_onclick.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final Boolean[] ch = {true};
                        mDatabase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                ArrayList<String> childKey_list = new ArrayList<>();
                                childKey_list.clear();
                                for (DataSnapshot p : dataSnapshot.getChildren()) {
                                    String childKey = p.getKey();
                                    childKey_list.add(childKey);
                                }
                                if (ch[0]) {
                                    String delete_childID = childKey_list.get(position);
                                    mDatabase.child(delete_childID).removeValue();
                                    ch[0] = false;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });
                dialog_onclick.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView row_pressure_max;
        final TextView row_pressure_heart;
        final TextView row_pressure_date;
        String time = null;

        public ViewHolder(View itemView) {
            super(itemView);
            row_pressure_max = (TextView) itemView.findViewById(R.id.row_pressure_max);
            row_pressure_heart = (TextView) itemView.findViewById(R.id.row_pressure_heart);
            row_pressure_date = (TextView) itemView.findViewById(R.id.row_pressure_date);
        }

        @SuppressLint("SetTextI18n")
        public void setValues(Contact contact) {
            String time = contact.getPressure_cdate_value();
            time = time.replace(" ", "\n");
            row_pressure_max.setText(contact.getMax_pressure_value() + "/" + contact.getMin_pressure_value());
            row_pressure_heart.setText(contact.getHeart_pressure_value());
            row_pressure_date.setText(time);
        }
    }
}
