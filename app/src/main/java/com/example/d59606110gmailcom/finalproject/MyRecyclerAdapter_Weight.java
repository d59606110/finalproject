package com.example.d59606110gmailcom.finalproject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MyRecyclerAdapter_Weight extends RecyclerView.Adapter<MyRecyclerAdapter_Weight.ViewHolder> {
    final ArrayList<Contact> list;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private String UID = null;
    final Boolean[] ch = {true};

    public MyRecyclerAdapter_Weight(ArrayList<Contact> list) {
        this.list = list;
    }

    @Override
    public MyRecyclerAdapter_Weight.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_weight, parent, false);
        MyRecyclerAdapter_Weight.ViewHolder viewHolder = new MyRecyclerAdapter_Weight.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Contact contact = list.get(position);
        holder.setValues(contact);

        auth = FirebaseAuth.getInstance();
        UID = auth.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference("user/" + UID + "/weightdata");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder dialog_onclick = new AlertDialog.Builder(v.getContext());
                dialog_onclick.setTitle("資料");
                dialog_onclick.setMessage("是否要刪除這筆資料？");
                dialog_onclick.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog_onclick.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mDatabase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                ArrayList<String> childKey_list = new ArrayList<>();
                                childKey_list.clear();
                                for (DataSnapshot p : dataSnapshot.getChildren()) {
                                    String childKey = p.getKey();
                                    childKey_list.add(childKey);
                                }
                                Log.i("TAG", String.valueOf(position));
                                if (ch[0]) {
                                    String delete_childID = childKey_list.get(position);
                                    mDatabase.child(delete_childID).removeValue();
                                    ch[0] = false;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                });
                dialog_onclick.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView row_weight_value;
        final TextView row_weight_time;
        String time = null, ww;
        double weight;

        public ViewHolder(View itemView) {
            super(itemView);
            row_weight_time = (TextView) itemView.findViewById(R.id.row_weight_time);
            row_weight_value = (TextView) itemView.findViewById(R.id.row_weight_value);
        }

        private void setValues(Contact contact) {
            String date = contact.getCdate();
            String dd[] = {"年", "月", "日"};
            ww = contact.getWeight();
            DecimalFormat df = new DecimalFormat("#.0");
            date = date.replaceFirst("-", dd[0]);
            date = date.replaceFirst("-", dd[1]);
            date = date.replace(" ", dd[2] + "\n");
            Log.v("TAG", date);
            weight = Double.parseDouble(df.format(Double.valueOf(ww)));
//            row_weight_time.setText(contact.getCdate());
            row_weight_time.setText(date);
            row_weight_value.setText(String.valueOf(weight));
        }
    }
}
