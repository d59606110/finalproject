package com.example.d59606110gmailcom.finalproject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

public class PressureActivity extends AppCompatActivity {
    EditText max_pressure_value, min_pressure_value, heart_pressure_value, pressure_cdate_value, pressure_ctime_value;
    Button btn_add_pressure, btn_inquire_pressure;
    String UID = null;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private int year, month, day, hour, minute;
    private String smonth, sday, si, sil;
    private int max = 120, min = 80, heart = 80;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pressure);

        /*左上角加入返回建*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_pressure);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        max_pressure_value = (EditText) findViewById(R.id.max_pressure_value);
        min_pressure_value = (EditText) findViewById(R.id.min_pressure_value);
        heart_pressure_value = (EditText) findViewById(R.id.heart_pressure_value);
        pressure_cdate_value = (EditText) findViewById(R.id.pressure_cdate_value);
        pressure_ctime_value = (EditText) findViewById(R.id.pressure_ctime_value);
        btn_add_pressure = (Button) findViewById(R.id.btn_add_pressure);
        btn_inquire_pressure = (Button) findViewById(R.id.btn_inquire_pressure);
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("user");

        UID = auth.getUid();

        getTime();
        /*日期統一格式輸入*/
        pressure_cdate_value.setInputType(InputType.TYPE_NULL);
        pressure_cdate_value.setText(year + "-" + smonth + "-" + sday);
        pressure_cdate_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showDatePickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*時間統一格式輸入*/
        pressure_ctime_value.setInputType(InputType.TYPE_NULL);
        pressure_ctime_value.setText(si + ":" + sil);
        pressure_ctime_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showTimePickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*收縮壓數值滑動輸入*/
        max_pressure_value.setInputType(InputType.TYPE_NULL);
        max_pressure_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showNumberPickerDialog_max();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*舒張壓數值滑動輸入*/
        min_pressure_value.setInputType(InputType.TYPE_NULL);
        min_pressure_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showNumberPickerDialog_min();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*心跳數值滑動輸入*/
        heart_pressure_value.setInputType(InputType.TYPE_NULL);
        heart_pressure_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showNumberPickerDialog_heart();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*血壓資料存進 Firebase Realtime Database*/
        btn_add_pressure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pressure_cdate_value.getText().toString().equals("")) {
                    pressure_cdate_value.setError("");
                    pressure_ctime_value.setError(null);
                    max_pressure_value.setError(null);
                    min_pressure_value.setError(null);
                    ErrorMsg();
                } else if (pressure_ctime_value.getText().toString().equals("")) {
                    pressure_ctime_value.setError("");
                    pressure_cdate_value.setError(null);
                    max_pressure_value.setError(null);
                    min_pressure_value.setError(null);
                    ErrorMsg();
                } else if (max_pressure_value.getText().toString().equals("")) {
                    pressure_cdate_value.setError(null);
                    pressure_ctime_value.setError(null);
                    max_pressure_value.setError("");
                    min_pressure_value.setError(null);
                    ErrorMsg();
                } else if (min_pressure_value.getText().toString().equals("")) {
                    pressure_cdate_value.setError(null);
                    pressure_ctime_value.setError(null);
                    max_pressure_value.setError(null);
                    min_pressure_value.setError("");
                    ErrorMsg();
                } else {
                    Contact user = new Contact(heart_pressure_value.getText().toString(),
                            max_pressure_value.getText().toString(),
                            min_pressure_value.getText().toString(),
                            (pressure_cdate_value.getText().toString() + " " + pressure_ctime_value.getText().toString()));
                    mDatabase.child(UID).child("blood_pressure").push().setValue(user);
                    AlertDialog.Builder dialog_ok = new AlertDialog.Builder(PressureActivity.this);
                    dialog_ok.setTitle("資料新增成功提示");
                    dialog_ok.setMessage("恭喜！資料新增成功");
                    dialog_ok.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialog_ok.show();
                    max_pressure_value.setText("");
                    min_pressure_value.setText("");
                    heart_pressure_value.setText("");
//                    pressure_cdate_value.setText("");
//                    pressure_ctime_value.setText("");
                    Log.v("TAG", "新增資料成功");
                }
            }
        });

        /*跳轉頁面到血壓查詢*/
        btn_inquire_pressure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(
                        PressureActivity.this,
                        InquirePressureActivity.class
                ), 1);
            }
        });
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(PressureActivity.this, DatePickerDialog.THEME_HOLO_DARK
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String smonth, sday, si, sil;
                if (monthOfYear >= 1 && monthOfYear <= 9) {
                    smonth = "0" + Integer.toString(monthOfYear);
                } else {
                    smonth = Integer.toString(monthOfYear);
                }
                if (dayOfMonth >= 1 && dayOfMonth <= 9) {
                    sday = "0" + Integer.toString(dayOfMonth);
                } else {
                    sday = Integer.toString(dayOfMonth);
                }
                pressure_cdate_value.setText(year + "-" + smonth + "-" + sday);
            }
        }, year, month, day).show();
    }

    private void ErrorMsg() {
        AlertDialog.Builder dialog_reset = new AlertDialog.Builder(PressureActivity.this);
        dialog_reset.setTitle("資料新增失敗");
        dialog_reset.setMessage("請在確認一次資料是否填寫完整");
        dialog_reset.setPositiveButton("再試一次", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog_reset.show();
    }

    private void showTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        new TimePickerDialog(PressureActivity.this, TimePickerDialog.THEME_HOLO_DARK
                , new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                String si, sil;
                if (i >= 0 && i <= 9) {
                    si = "0" + Integer.toString(i);
                } else {
                    si = Integer.toString(i);
                }
                if (i1 >= 0 && i1 <= 9) {
                    sil = "0" + Integer.toString(i1);
                } else {
                    sil = Integer.toString(i1);
                }
                pressure_ctime_value.setText(si + ":" + sil);
            }
        }, hour, minute, false).show();
    }

    public void showNumberPickerDialog_max() {
        final AlertDialog.Builder number_dialog = new AlertDialog.Builder(PressureActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        number_dialog.setTitle("收縮壓")
                .setView(dialogView);
        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.dialog_number_picker);
        numberPicker.setMaxValue(300);
        numberPicker.setMinValue(80);
        numberPicker.setValue(max);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                max = newVal;
            }
        });
        number_dialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                max_pressure_value.setText(String.valueOf(max));
            }
        });
        number_dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        number_dialog.show();
    }

    public void showNumberPickerDialog_min() {
        final AlertDialog.Builder number_dialog = new AlertDialog.Builder(PressureActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        number_dialog.setTitle("舒張壓")
                .setView(dialogView);
        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.dialog_number_picker);
        numberPicker.setMaxValue(200);
        numberPicker.setMinValue(0);
        numberPicker.setValue(min);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                min = newVal;
            }
        });
        number_dialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                min_pressure_value.setText(String.valueOf(min));
            }
        });
        number_dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        number_dialog.show();
    }

    public void showNumberPickerDialog_heart() {
        final AlertDialog.Builder number_dialog = new AlertDialog.Builder(PressureActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        number_dialog.setTitle("脈搏")
                .setView(dialogView);
        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.dialog_number_picker);
        numberPicker.setMaxValue(200);
        numberPicker.setMinValue(0);
        numberPicker.setValue(heart);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                heart = newVal;
            }
        });
        number_dialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                heart_pressure_value.setText(String.valueOf(heart));
            }
        });
        number_dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        number_dialog.show();
    }

    private void getTime() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        if (month >= 1 && month <= 9) {
            smonth = "0" + Integer.toString(month);
        } else {
            smonth = Integer.toString(month);
        }
        if (day >= 1 && day <= 9) {
            sday = "0" + Integer.toString(day);
        } else {
            sday = Integer.toString(day);
        }
        if (hour >= 0 && hour <= 9) {
            si = "0" + Integer.toString(hour);
        } else {
            si = Integer.toString(hour);
        }
        if (minute >= 0 && minute <= 9) {
            sil = "0" + Integer.toString(minute);
        } else {
            sil = Integer.toString(minute);
        }
    }
}
