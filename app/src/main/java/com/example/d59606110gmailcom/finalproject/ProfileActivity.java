package com.example.d59606110gmailcom.finalproject;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ProfileActivity extends AppCompatActivity {
    TextView member_name;
    Button btn_edit, btn_signout;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth auth;
    private ArrayAdapter<String> adapter;
    private String UID = null;
    int age = 0;
    String name, user_name;
    String a_HbA1c = null, a_birthday = null, a_height = null,
            a_name = null, a_sex = null, a_sick_type = null, a_weight = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        /*左上角加入返回鍵*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_profile);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        member_name = (TextView) findViewById(R.id.member_name);
        btn_edit = (Button) findViewById(R.id.btn_edit);
        btn_signout = (Button) findViewById(R.id.signout_btn);

        auth = FirebaseAuth.getInstance();
        UID = auth.getUid();
        user_name = auth.getCurrentUser().getDisplayName();
        DatabaseReference reference_contacts = FirebaseDatabase.getInstance().getReference("user/" + UID);


        /*顯示會員名字*/
        ListView listView = (ListView) findViewById(R.id.list_profile);
        adapter = new ArrayAdapter<String>(ProfileActivity.this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1);
        listView.setAdapter(adapter);


        reference_contacts.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    name = dataSnapshot.child("name").getValue().toString();
                    Log.d("TAG", "姓名：" + name);
                    member_name.setText(name);

                    /*計算年齡*/
                    String dataOfBirth = dataSnapshot.child("birthday").getValue().toString();

                    if (!dataOfBirth.equals("")) {
                        age = getAge(parse(dataOfBirth));
                    }

                    /*儲存資料庫的值*/
                    String key = null;
                    a_birthday = dataSnapshot.child("birthday").getValue().toString();
                    a_name = dataSnapshot.child("name").getValue().toString();
                    a_sex = dataSnapshot.child("sex").getValue().toString();
                    a_sick_type = dataSnapshot.child("sick_type").getValue().toString();
                    a_height = dataSnapshot.child("height").getValue().toString();
                    for (DataSnapshot p : dataSnapshot.child("weightdata").getChildren()) {
                        key = p.getKey();
                    }
                    a_weight = dataSnapshot.child("weightdata").child(key).child("weight").getValue().toString();
                    Log.i("TAG", a_weight);

                    /*計算BMI*/
                    double weight, height, BMI;
                    DecimalFormat df = new DecimalFormat("#.00");
                    height = Double.parseDouble(dataSnapshot.child("height").getValue().toString());
                    BMI = Double.valueOf(a_weight) / Math.pow((height / 100), 2);

                    /*資料呈現*/
                    adapter.clear();
                    adapter.add("性別：" + dataSnapshot.child("sex").getValue().toString());
                    adapter.add("年齡：" + age);
                    adapter.add("生日：" + dataSnapshot.child("birthday").getValue().toString());
                    adapter.add("身高：" + dataSnapshot.child("height").getValue().toString() + " cm");
                    adapter.add("體重：" + a_weight + " kg");
                    adapter.add("BMI：" + df.format(BMI));
                    adapter.add("糖尿病類型：" + dataSnapshot.child("sick_type").getValue().toString());
                    adapter.add("糖化血色素：" + dataSnapshot.child("HbA1c").getValue().toString());
                } catch (Exception e) {
                    startActivity(new Intent(ProfileActivity.this, EditProfileActivity.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        /*跳轉頁面到編輯會員資料*/
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ProfileActivity.this,
                        EditProfileActivity.class), 1);
            }
        });

//        登出會員
        btn_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthUI.getInstance()
                        .signOut(ProfileActivity.this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                finish();
                            }
                        });
            }
        });
    }

    public int getAge(Date birthDay) throws Exception {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) {
            throw new IllegalArgumentException("The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;
            } else {
                age--;
            }
        }
        return age;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public Date parse(String strDate) throws IllegalArgumentException, ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(strDate);
    }


    //出生日期字符串转化成Date对象
//    public Date parse(String strDate) throws ParseException {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        return sdf.parse(strDate);
//    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        Log.i("TAG", "返回鍵");
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if (a_birthday.equals("") || a_height.equals("") || a_name.equals("") || a_sex.equals("") ||
//                    a_sick_type.equals("") || a_weight.equals("")) {
//                AlertDialog.Builder msg = new AlertDialog.Builder(ProfileActivity.this);
//                msg.setTitle("請確認資料是否填寫完畢");
//                msg.setMessage("資料尚有未填寫");
//                msg.setNegativeButton("再次填寫", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//                msg.show();
//            }
//        }
//        return super.onKeyDown(keyCode, event);
//    }
}
