package com.example.d59606110gmailcom.finalproject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

public class SugarActivity extends AppCompatActivity {
    Spinner type_item;
    EditText blood_sugar_values, date_values, time_values;
    Button btn_add, btn_inquire;
    String UID = null;
    int blood_sugar = 100;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private int year, month, day, hour, minute;
    private String smonth, sday, si, sil;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugar);

        /*左上角加入返回鍵*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_sugar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*給予參數*/
        auth = FirebaseAuth.getInstance();
        UID = auth.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference("user");
        date_values = (EditText) findViewById(R.id.date_values);
        time_values = (EditText) findViewById(R.id.time_values);
        type_item = (Spinner) findViewById(R.id.type_item);
        blood_sugar_values = (EditText) findViewById(R.id.blood_suger_values);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_inquire = (Button) findViewById(R.id.btn_inquire);

        getTime();
        /*日期統一格式輸入*/
        date_values.setInputType(InputType.TYPE_NULL);
        date_values.setText(year + "-" + smonth + "-" + sday);
        date_values.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showDatePickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*時間統一格式輸入*/
        time_values.setInputType(InputType.TYPE_NULL);
        time_values.setText(si + ":" + sil);
        time_values.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showTimePickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*血糖值滑動輸入*/
        blood_sugar_values.setInputType(InputType.TYPE_NULL);
        blood_sugar_values.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showNumberPickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*將陣列內的數值放到Spinner內並且呈現出來*/
        String type[] = {"早餐前", "早餐後", "午餐前", "午餐後", "晚餐前", "晚餐後", "睡覺前"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SugarActivity.this,
                R.layout.support_simple_spinner_dropdown_item,
                type);
        type_item.setAdapter(adapter);

        /*資料新增進去資料庫*/
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*如果有資料空的跳出訊息提示*/
                if (date_values.getText().toString().equals("")
                        || time_values.getText().toString().equals("")
                        || blood_sugar_values.getText().toString().equals("")) {
                    AlertDialog.Builder dialog_reset = new AlertDialog.Builder(SugarActivity.this);
                    dialog_reset.setTitle("資料新增失敗");
                    dialog_reset.setMessage("請在確認一次資料是否填寫完整");
                    dialog_reset.setPositiveButton("再試一次", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialog_reset.show();
                } else {
                    /*將資料存進Firebase Realtime Database*/
                    Contact user = new Contact((date_values.getText().toString() + " " + time_values.getText().toString()),
                            type_item.getSelectedItem().toString(),
                            blood_sugar_values.getText().toString());
                    mDatabase.child(UID).child("blood_sugar").push().setValue(user);
                    AlertDialog.Builder dialog_ok = new AlertDialog.Builder(SugarActivity.this);
                    dialog_ok.setTitle("資料新增成功提示");
                    dialog_ok.setMessage("恭喜！資料新增成功");
                    dialog_ok.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialog_ok.show();
//                    date_values.setText("");
//                    time_values.setText("");
                    blood_sugar_values.setText("");
                    Log.v("TAG", "新增資料成功");
                }
            }
        });
        /*切換頁面到查詢資料*/
        btn_inquire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SugarActivity.this,
                        InquireSugarActivity.class), 1);
                onPause();
            }
        });
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(SugarActivity.this, DatePickerDialog.THEME_HOLO_DARK
                , new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String smonth, sday, si, sil;
                if (monthOfYear >= 1 && monthOfYear <= 9) {
                    smonth = "0" + Integer.toString(monthOfYear);
                } else {
                    smonth = Integer.toString(monthOfYear);
                }
                if (dayOfMonth >= 1 && dayOfMonth <= 9) {
                    sday = "0" + Integer.toString(dayOfMonth);
                } else {
                    sday = Integer.toString(dayOfMonth);
                }
                date_values.setText(year + "-" + smonth + "-" + sday);
            }
        }, year, month, day).show();
    }

    private void showTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        new TimePickerDialog(SugarActivity.this, TimePickerDialog.THEME_HOLO_DARK
                , new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                String si, sil;
                if (i >= 0 && i <= 9) {
                    si = "0" + Integer.toString(i);
                } else {
                    si = Integer.toString(i);
                }
                if (i1 >= 0 && i1 <= 9) {
                    sil = "0" + Integer.toString(i1);
                } else {
                    sil = Integer.toString(i1);
                }
                time_values.setText(si + ":" + sil);
            }
        }, hour, minute, false).show();
    }

    public void showNumberPickerDialog() {
        final AlertDialog.Builder number_dialog = new AlertDialog.Builder(SugarActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        number_dialog.setTitle("血糖值")
                .setView(dialogView);
        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.dialog_number_picker);
        numberPicker.setMaxValue(999);
        numberPicker.setMinValue(0);
        numberPicker.setValue(blood_sugar);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                blood_sugar = newVal;
            }
        });
        number_dialog.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                blood_sugar_values.setText(String.valueOf(blood_sugar));
            }
        });
        number_dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        number_dialog.show();
    }

    private void getTime() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        if (month >= 1 && month <= 9) {
            smonth = "0" + Integer.toString(month);
        } else {
            smonth = Integer.toString(month);
        }
        if (day >= 1 && day <= 9) {
            sday = "0" + Integer.toString(day);
        } else {
            sday = Integer.toString(day);
        }
        if (hour >= 0 && hour <= 9) {
            si = "0" + Integer.toString(hour);
        } else {
            si = Integer.toString(hour);
        }
        if (minute >= 0 && minute <= 9) {
            sil = "0" + Integer.toString(minute);
        } else {
            sil = Integer.toString(minute);
        }
    }
}
