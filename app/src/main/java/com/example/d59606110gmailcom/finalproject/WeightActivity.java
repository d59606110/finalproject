package com.example.d59606110gmailcom.finalproject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class WeightActivity extends AppCompatActivity {
    EditText weight_date, weight_time, weight_values, weight_fat;
    Button btn_add_weight, btn_inquire_weight;
    String UID = null;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private int year, month, day, hour, minute;
    private String smonth, sday, si, sil;
    int integer = 60, fraction = 0, fat_int = 15, fat_fra = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);

        /*左上角加入返回鍵*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_weight);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        auth = FirebaseAuth.getInstance();
        UID = auth.getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference("user");
        Log.i("TAG", "Login:" + UID);

        weight_date = (EditText) findViewById(R.id.weight_date);
        weight_time = (EditText) findViewById(R.id.weight_time);
        weight_values = (EditText) findViewById(R.id.weight_values);
        weight_fat = (EditText) findViewById(R.id.weight_fat);
        btn_add_weight = (Button) findViewById(R.id.btn_add_weight);
        btn_inquire_weight = (Button) findViewById(R.id.btn_inquire_weight);

        getTime();
        /*日期統一格式輸入*/
        weight_date.setInputType(InputType.TYPE_NULL);
        weight_date.setText(year + "-" + smonth + "-" + sday);
        weight_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showDatePickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*時間統一格式輸入*/
        weight_time.setInputType(InputType.TYPE_NULL);
        weight_time.setText(si + ":" + sil);
        weight_time.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        showTimePickerDialog();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*體重滑動輸入*/
        weight_values.setInputType(InputType.TYPE_NULL);
        weight_values.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Weight_values();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*體脂肪滑動輸入*/
        weight_fat.setInputType(InputType.TYPE_NULL);
        weight_fat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Fat_value();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });

        /*監聽新增按鈕*/
        btn_add_weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fat;
                if (weight_time.getText().toString().equals("")) {
                    weight_time.setError("");
                } else if (weight_values.getText().toString().equals("")) {
                    weight_values.setError("");
                } else {
                    if (weight_fat.getText().toString().equals("")) {
                        fat = "無";
                    } else {
                        fat = weight_fat.getText().toString();
                    }
                    Log.i("TAG", weight_time.getText().toString());
                    Log.i("TAG", weight_values.getText().toString());
                    Contact userWeight = new Contact((weight_date.getText().toString() + " " + weight_time.getText().toString()),
                            weight_values.getText().toString(), fat, 0, 0, 0);
                    mDatabase.child(UID).child("weightdata").push().setValue(userWeight);
                    Map<String, Object> data = new HashMap<>();
                    data.put("weight", weight_values.getText().toString());
                    mDatabase.child(UID).updateChildren(data);
                    inDataOk();
                    Log.i("TAG", auth.getCurrentUser().getDisplayName() + "新增資料成功");
//                    weight_date.setText("");
//                    weight_time.setText("");
                    weight_values.setText("");
                    weight_fat.setText("");
                }
            }
        });

        /*監聽查詢按鈕*/
        btn_inquire_weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WeightActivity.this, InquireWeightActivity.class));
            }
        });
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(WeightActivity.this, DatePickerDialog.THEME_HOLO_DARK
                , new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear += 1;
                String smonth, sday, si, sil;
                if (monthOfYear >= 1 && monthOfYear <= 9) {
                    smonth = "0" + Integer.toString(monthOfYear);
                } else {
                    smonth = Integer.toString(monthOfYear);
                }
                if (dayOfMonth >= 1 && dayOfMonth <= 9) {
                    sday = "0" + Integer.toString(dayOfMonth);
                } else {
                    sday = Integer.toString(dayOfMonth);
                }
                weight_date.setText(year + "-" + smonth + "-" + sday);
            }
        }, year, month, day).show();
    }

    private void showTimePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        new TimePickerDialog(WeightActivity.this, TimePickerDialog.THEME_HOLO_DARK
                , new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                String si, sil;
                if (i >= 0 && i <= 9) {
                    si = "0" + Integer.toString(i);
                } else {
                    si = Integer.toString(i);
                }
                if (i1 >= 0 && i1 <= 9) {
                    sil = "0" + Integer.toString(i1);
                } else {
                    sil = Integer.toString(i1);
                }
                weight_time.setText(si + ":" + sil);
            }
        }, hour, minute, false).show();
    }

    private void inDataOk() {
        AlertDialog.Builder dialog_ok = new AlertDialog.Builder(WeightActivity.this);
        dialog_ok.setTitle("資料新增成功提示");
        dialog_ok.setMessage("恭喜！資料新增成功");
        dialog_ok.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog_ok.show();
    }

    private void Weight_values() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WeightActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.double_number_picker, null);

        /*宣告Layout變數*/
        final NumberPicker integer_picker = (NumberPicker) view.findViewById(R.id.integer_picker);
        final NumberPicker fraction_picker = (NumberPicker) view.findViewById(R.id.fraction_picker);

        builder.setTitle("體重").setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        weight_values.setText(String.valueOf(integer) + "." + String.valueOf(fraction));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        /*整數部分設置*/
        integer_picker.setMaxValue(400);
        integer_picker.setMinValue(0);
        integer_picker.setValue(integer);
        integer_picker.setWrapSelectorWheel(false);

        /*小數部分設置*/
        fraction_picker.setMaxValue(9);
        fraction_picker.setMinValue(0);
        fraction_picker.setValue(fraction);

        integer_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                integer = newVal;
            }
        });
        fraction_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                fraction = newVal;
            }
        });
        builder.show();
    }

    private void Fat_value() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WeightActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.double_number_picker, null);

        /*宣告Layout變數*/
        final NumberPicker integer_picker = (NumberPicker) view.findViewById(R.id.integer_picker);
        final NumberPicker fraction_picker = (NumberPicker) view.findViewById(R.id.fraction_picker);

        builder.setTitle("體脂肪").setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        weight_fat.setText(String.valueOf(fat_int) + "." + String.valueOf(fat_fra));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        /*整數部分設置*/
        integer_picker.setMaxValue(40);
        integer_picker.setMinValue(0);
        integer_picker.setValue(fat_int);
        integer_picker.setWrapSelectorWheel(false);

        /*小數部分設置*/
        fraction_picker.setMaxValue(9);
        fraction_picker.setMinValue(0);
        fraction_picker.setValue(fat_fra);

        integer_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                fat_int = newVal;
            }
        });
        fraction_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                fat_fra = newVal;
            }
        });
        builder.show();
    }

    private void getTime() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        if (month >= 1 && month <= 9) {
            smonth = "0" + Integer.toString(month);
        } else {
            smonth = Integer.toString(month);
        }
        if (day >= 1 && day <= 9) {
            sday = "0" + Integer.toString(day);
        } else {
            sday = Integer.toString(day);
        }
        if (hour >= 0 && hour <= 9) {
            si = "0" + Integer.toString(hour);
        } else {
            si = Integer.toString(hour);
        }
        if (minute >= 0 && minute <= 9) {
            sil = "0" + Integer.toString(minute);
        } else {
            sil = Integer.toString(minute);
        }
    }
}

